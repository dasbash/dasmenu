INSTALL_DIR=~/.scripts
CURRENT_PATH=$(shell pwd)

all:
	@echo "Please run 'make install'"
install:
	@echo ''
	mkdir -p $(INSTALL_DIR)
	ln -s -i $(CURRENT_PATH)/dasmenu $(INSTALL_DIR)/dasmenu
	@echo ''
	@echo 'USAGE:'
	@echo '------'
	@echo 'dasmenu'
	@echo 'dasmenu <optional_menu_path>'

.PHONY: all install
