# Dasmenu

## Background

This is a simple script built around `dmenu` to allow you to build a dynamic menu.  For my set-up I have configured a `Meta` + `x` as a shortcut in `i3wm` to show the menu.  The benefit of this is I can quickly build a useful menu with options I like to use frequently - like making backups, or recording accounts transactions. Anything really.

The menu simply displays files and directories as options, and runs executables or recurses into the selected directory and presents a new menu from there.  Executables may also present their own menu using `dmenu` which results in a dynamic and focused menu.

It's also possible, with patches in `dmenu` to update it so options are selected automatically when there is only one option to show.  This may or may not be desired, so that's up to you to add or not.

## Prerequisites

* dmenu
* bash

There are other applications that may be used in the menu options available by default, but these are purely what I use and are completely optional.

## Installation

To install this, to `~/.scripts/`:

```sh
make install
```

Alternatively, as it's a simple script, you can simply symlink `dasmenu` to your chosen bin path:

```bash
ln -s /path/to/dasmenu ~/.local/bin/dasmenu
```
